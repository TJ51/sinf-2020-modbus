#
# Generated Makefile - do not edit!
#
# Edit the Makefile in the project folder instead (../Makefile). Each target
# has a -pre and a -post target defined where you can add customized code.
#
# This makefile implements configuration specific macros and targets.


# Include project Makefile
ifeq "${IGNORE_LOCAL}" "TRUE"
# do not include local makefile. User is passing all local related variables already
else
include Makefile
# Include makefile containing local settings
ifeq "$(wildcard nbproject/Makefile-local-default.mk)" "nbproject/Makefile-local-default.mk"
include nbproject/Makefile-local-default.mk
endif
endif

# Environment
MKDIR=gnumkdir -p
RM=rm -f 
MV=mv 
CP=cp 

# Macros
CND_CONF=default
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
IMAGE_TYPE=debug
OUTPUT_SUFFIX=elf
DEBUGGABLE_SUFFIX=elf
FINAL_IMAGE=dist/${CND_CONF}/${IMAGE_TYPE}/MyModbusServer.X.${IMAGE_TYPE}.${OUTPUT_SUFFIX}
else
IMAGE_TYPE=production
OUTPUT_SUFFIX=hex
DEBUGGABLE_SUFFIX=elf
FINAL_IMAGE=dist/${CND_CONF}/${IMAGE_TYPE}/MyModbusServer.X.${IMAGE_TYPE}.${OUTPUT_SUFFIX}
endif

ifeq ($(COMPARE_BUILD), true)
COMPARISON_BUILD=--mafrlcsj
else
COMPARISON_BUILD=
endif

ifdef SUB_IMAGE_ADDRESS

else
SUB_IMAGE_ADDRESS_COMMAND=
endif

# Object Directory
OBJECTDIR=build/${CND_CONF}/${IMAGE_TYPE}

# Distribution Directory
DISTDIR=dist/${CND_CONF}/${IMAGE_TYPE}

# Source Files Quoted if spaced
SOURCEFILES_QUOTED_IF_SPACED=../moule_picebs2_students/adc.c ../moule_picebs2_students/arialNarrow_12ptBitmaps.c ../moule_picebs2_students/arialRoundedMTBold18ptBitmaps.c ../moule_picebs2_students/configuration_bits.c ../moule_picebs2_students/crc.c ../moule_picebs2_students/eeprom.c ../moule_picebs2_students/interrupts.c ../moule_picebs2_students/lcd_highlevel.c ../moule_picebs2_students/lcd_lowlevel.c ../moule_picebs2_students/main.c ../moule_picebs2_students/modbus.c ../moule_picebs2_students/pwm.c ../moule_picebs2_students/tsc.c ../moule_picebs2_students/uart.c

# Object Files Quoted if spaced
OBJECTFILES_QUOTED_IF_SPACED=${OBJECTDIR}/_ext/2141833485/adc.p1 ${OBJECTDIR}/_ext/2141833485/arialNarrow_12ptBitmaps.p1 ${OBJECTDIR}/_ext/2141833485/arialRoundedMTBold18ptBitmaps.p1 ${OBJECTDIR}/_ext/2141833485/configuration_bits.p1 ${OBJECTDIR}/_ext/2141833485/crc.p1 ${OBJECTDIR}/_ext/2141833485/eeprom.p1 ${OBJECTDIR}/_ext/2141833485/interrupts.p1 ${OBJECTDIR}/_ext/2141833485/lcd_highlevel.p1 ${OBJECTDIR}/_ext/2141833485/lcd_lowlevel.p1 ${OBJECTDIR}/_ext/2141833485/main.p1 ${OBJECTDIR}/_ext/2141833485/modbus.p1 ${OBJECTDIR}/_ext/2141833485/pwm.p1 ${OBJECTDIR}/_ext/2141833485/tsc.p1 ${OBJECTDIR}/_ext/2141833485/uart.p1
POSSIBLE_DEPFILES=${OBJECTDIR}/_ext/2141833485/adc.p1.d ${OBJECTDIR}/_ext/2141833485/arialNarrow_12ptBitmaps.p1.d ${OBJECTDIR}/_ext/2141833485/arialRoundedMTBold18ptBitmaps.p1.d ${OBJECTDIR}/_ext/2141833485/configuration_bits.p1.d ${OBJECTDIR}/_ext/2141833485/crc.p1.d ${OBJECTDIR}/_ext/2141833485/eeprom.p1.d ${OBJECTDIR}/_ext/2141833485/interrupts.p1.d ${OBJECTDIR}/_ext/2141833485/lcd_highlevel.p1.d ${OBJECTDIR}/_ext/2141833485/lcd_lowlevel.p1.d ${OBJECTDIR}/_ext/2141833485/main.p1.d ${OBJECTDIR}/_ext/2141833485/modbus.p1.d ${OBJECTDIR}/_ext/2141833485/pwm.p1.d ${OBJECTDIR}/_ext/2141833485/tsc.p1.d ${OBJECTDIR}/_ext/2141833485/uart.p1.d

# Object Files
OBJECTFILES=${OBJECTDIR}/_ext/2141833485/adc.p1 ${OBJECTDIR}/_ext/2141833485/arialNarrow_12ptBitmaps.p1 ${OBJECTDIR}/_ext/2141833485/arialRoundedMTBold18ptBitmaps.p1 ${OBJECTDIR}/_ext/2141833485/configuration_bits.p1 ${OBJECTDIR}/_ext/2141833485/crc.p1 ${OBJECTDIR}/_ext/2141833485/eeprom.p1 ${OBJECTDIR}/_ext/2141833485/interrupts.p1 ${OBJECTDIR}/_ext/2141833485/lcd_highlevel.p1 ${OBJECTDIR}/_ext/2141833485/lcd_lowlevel.p1 ${OBJECTDIR}/_ext/2141833485/main.p1 ${OBJECTDIR}/_ext/2141833485/modbus.p1 ${OBJECTDIR}/_ext/2141833485/pwm.p1 ${OBJECTDIR}/_ext/2141833485/tsc.p1 ${OBJECTDIR}/_ext/2141833485/uart.p1

# Source Files
SOURCEFILES=../moule_picebs2_students/adc.c ../moule_picebs2_students/arialNarrow_12ptBitmaps.c ../moule_picebs2_students/arialRoundedMTBold18ptBitmaps.c ../moule_picebs2_students/configuration_bits.c ../moule_picebs2_students/crc.c ../moule_picebs2_students/eeprom.c ../moule_picebs2_students/interrupts.c ../moule_picebs2_students/lcd_highlevel.c ../moule_picebs2_students/lcd_lowlevel.c ../moule_picebs2_students/main.c ../moule_picebs2_students/modbus.c ../moule_picebs2_students/pwm.c ../moule_picebs2_students/tsc.c ../moule_picebs2_students/uart.c


CFLAGS=
ASFLAGS=
LDLIBSOPTIONS=

############# Tool locations ##########################################
# If you copy a project from one host to another, the path where the  #
# compiler is installed may be different.                             #
# If you open this project with MPLAB X in the new host, this         #
# makefile will be regenerated and the paths will be corrected.       #
#######################################################################
# fixDeps replaces a bunch of sed/cat/printf statements that slow down the build
FIXDEPS=fixDeps

.build-conf:  ${BUILD_SUBPROJECTS}
ifneq ($(INFORMATION_MESSAGE), )
	@echo $(INFORMATION_MESSAGE)
endif
	${MAKE}  -f nbproject/Makefile-default.mk dist/${CND_CONF}/${IMAGE_TYPE}/MyModbusServer.X.${IMAGE_TYPE}.${OUTPUT_SUFFIX}

MP_PROCESSOR_OPTION=18F87K22
# ------------------------------------------------------------------------------------
# Rules for buildStep: compile
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
${OBJECTDIR}/_ext/2141833485/adc.p1: ../moule_picebs2_students/adc.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/2141833485" 
	@${RM} ${OBJECTDIR}/_ext/2141833485/adc.p1.d 
	@${RM} ${OBJECTDIR}/_ext/2141833485/adc.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  -D__DEBUG=1  --debugger=pickit3  --double=24 --float=24 --emi=wordwrite --opt=+asm,+asmfile,-speed,+space,-debug,-local --addrqual=ignore --mode=free -P -N255 --warn=0 --asmlist -DXPRJ_default=$(CND_CONF)  --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,-plib $(COMPARISON_BUILD)  --output=-mcof,+elf:multilocs --stack=compiled:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"     -o${OBJECTDIR}/_ext/2141833485/adc.p1 ../moule_picebs2_students/adc.c 
	@-${MV} ${OBJECTDIR}/_ext/2141833485/adc.d ${OBJECTDIR}/_ext/2141833485/adc.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/_ext/2141833485/adc.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/_ext/2141833485/arialNarrow_12ptBitmaps.p1: ../moule_picebs2_students/arialNarrow_12ptBitmaps.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/2141833485" 
	@${RM} ${OBJECTDIR}/_ext/2141833485/arialNarrow_12ptBitmaps.p1.d 
	@${RM} ${OBJECTDIR}/_ext/2141833485/arialNarrow_12ptBitmaps.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  -D__DEBUG=1  --debugger=pickit3  --double=24 --float=24 --emi=wordwrite --opt=+asm,+asmfile,-speed,+space,-debug,-local --addrqual=ignore --mode=free -P -N255 --warn=0 --asmlist -DXPRJ_default=$(CND_CONF)  --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,-plib $(COMPARISON_BUILD)  --output=-mcof,+elf:multilocs --stack=compiled:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"     -o${OBJECTDIR}/_ext/2141833485/arialNarrow_12ptBitmaps.p1 ../moule_picebs2_students/arialNarrow_12ptBitmaps.c 
	@-${MV} ${OBJECTDIR}/_ext/2141833485/arialNarrow_12ptBitmaps.d ${OBJECTDIR}/_ext/2141833485/arialNarrow_12ptBitmaps.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/_ext/2141833485/arialNarrow_12ptBitmaps.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/_ext/2141833485/arialRoundedMTBold18ptBitmaps.p1: ../moule_picebs2_students/arialRoundedMTBold18ptBitmaps.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/2141833485" 
	@${RM} ${OBJECTDIR}/_ext/2141833485/arialRoundedMTBold18ptBitmaps.p1.d 
	@${RM} ${OBJECTDIR}/_ext/2141833485/arialRoundedMTBold18ptBitmaps.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  -D__DEBUG=1  --debugger=pickit3  --double=24 --float=24 --emi=wordwrite --opt=+asm,+asmfile,-speed,+space,-debug,-local --addrqual=ignore --mode=free -P -N255 --warn=0 --asmlist -DXPRJ_default=$(CND_CONF)  --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,-plib $(COMPARISON_BUILD)  --output=-mcof,+elf:multilocs --stack=compiled:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"     -o${OBJECTDIR}/_ext/2141833485/arialRoundedMTBold18ptBitmaps.p1 ../moule_picebs2_students/arialRoundedMTBold18ptBitmaps.c 
	@-${MV} ${OBJECTDIR}/_ext/2141833485/arialRoundedMTBold18ptBitmaps.d ${OBJECTDIR}/_ext/2141833485/arialRoundedMTBold18ptBitmaps.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/_ext/2141833485/arialRoundedMTBold18ptBitmaps.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/_ext/2141833485/configuration_bits.p1: ../moule_picebs2_students/configuration_bits.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/2141833485" 
	@${RM} ${OBJECTDIR}/_ext/2141833485/configuration_bits.p1.d 
	@${RM} ${OBJECTDIR}/_ext/2141833485/configuration_bits.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  -D__DEBUG=1  --debugger=pickit3  --double=24 --float=24 --emi=wordwrite --opt=+asm,+asmfile,-speed,+space,-debug,-local --addrqual=ignore --mode=free -P -N255 --warn=0 --asmlist -DXPRJ_default=$(CND_CONF)  --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,-plib $(COMPARISON_BUILD)  --output=-mcof,+elf:multilocs --stack=compiled:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"     -o${OBJECTDIR}/_ext/2141833485/configuration_bits.p1 ../moule_picebs2_students/configuration_bits.c 
	@-${MV} ${OBJECTDIR}/_ext/2141833485/configuration_bits.d ${OBJECTDIR}/_ext/2141833485/configuration_bits.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/_ext/2141833485/configuration_bits.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/_ext/2141833485/crc.p1: ../moule_picebs2_students/crc.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/2141833485" 
	@${RM} ${OBJECTDIR}/_ext/2141833485/crc.p1.d 
	@${RM} ${OBJECTDIR}/_ext/2141833485/crc.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  -D__DEBUG=1  --debugger=pickit3  --double=24 --float=24 --emi=wordwrite --opt=+asm,+asmfile,-speed,+space,-debug,-local --addrqual=ignore --mode=free -P -N255 --warn=0 --asmlist -DXPRJ_default=$(CND_CONF)  --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,-plib $(COMPARISON_BUILD)  --output=-mcof,+elf:multilocs --stack=compiled:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"     -o${OBJECTDIR}/_ext/2141833485/crc.p1 ../moule_picebs2_students/crc.c 
	@-${MV} ${OBJECTDIR}/_ext/2141833485/crc.d ${OBJECTDIR}/_ext/2141833485/crc.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/_ext/2141833485/crc.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/_ext/2141833485/eeprom.p1: ../moule_picebs2_students/eeprom.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/2141833485" 
	@${RM} ${OBJECTDIR}/_ext/2141833485/eeprom.p1.d 
	@${RM} ${OBJECTDIR}/_ext/2141833485/eeprom.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  -D__DEBUG=1  --debugger=pickit3  --double=24 --float=24 --emi=wordwrite --opt=+asm,+asmfile,-speed,+space,-debug,-local --addrqual=ignore --mode=free -P -N255 --warn=0 --asmlist -DXPRJ_default=$(CND_CONF)  --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,-plib $(COMPARISON_BUILD)  --output=-mcof,+elf:multilocs --stack=compiled:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"     -o${OBJECTDIR}/_ext/2141833485/eeprom.p1 ../moule_picebs2_students/eeprom.c 
	@-${MV} ${OBJECTDIR}/_ext/2141833485/eeprom.d ${OBJECTDIR}/_ext/2141833485/eeprom.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/_ext/2141833485/eeprom.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/_ext/2141833485/interrupts.p1: ../moule_picebs2_students/interrupts.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/2141833485" 
	@${RM} ${OBJECTDIR}/_ext/2141833485/interrupts.p1.d 
	@${RM} ${OBJECTDIR}/_ext/2141833485/interrupts.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  -D__DEBUG=1  --debugger=pickit3  --double=24 --float=24 --emi=wordwrite --opt=+asm,+asmfile,-speed,+space,-debug,-local --addrqual=ignore --mode=free -P -N255 --warn=0 --asmlist -DXPRJ_default=$(CND_CONF)  --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,-plib $(COMPARISON_BUILD)  --output=-mcof,+elf:multilocs --stack=compiled:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"     -o${OBJECTDIR}/_ext/2141833485/interrupts.p1 ../moule_picebs2_students/interrupts.c 
	@-${MV} ${OBJECTDIR}/_ext/2141833485/interrupts.d ${OBJECTDIR}/_ext/2141833485/interrupts.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/_ext/2141833485/interrupts.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/_ext/2141833485/lcd_highlevel.p1: ../moule_picebs2_students/lcd_highlevel.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/2141833485" 
	@${RM} ${OBJECTDIR}/_ext/2141833485/lcd_highlevel.p1.d 
	@${RM} ${OBJECTDIR}/_ext/2141833485/lcd_highlevel.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  -D__DEBUG=1  --debugger=pickit3  --double=24 --float=24 --emi=wordwrite --opt=+asm,+asmfile,-speed,+space,-debug,-local --addrqual=ignore --mode=free -P -N255 --warn=0 --asmlist -DXPRJ_default=$(CND_CONF)  --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,-plib $(COMPARISON_BUILD)  --output=-mcof,+elf:multilocs --stack=compiled:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"     -o${OBJECTDIR}/_ext/2141833485/lcd_highlevel.p1 ../moule_picebs2_students/lcd_highlevel.c 
	@-${MV} ${OBJECTDIR}/_ext/2141833485/lcd_highlevel.d ${OBJECTDIR}/_ext/2141833485/lcd_highlevel.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/_ext/2141833485/lcd_highlevel.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/_ext/2141833485/lcd_lowlevel.p1: ../moule_picebs2_students/lcd_lowlevel.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/2141833485" 
	@${RM} ${OBJECTDIR}/_ext/2141833485/lcd_lowlevel.p1.d 
	@${RM} ${OBJECTDIR}/_ext/2141833485/lcd_lowlevel.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  -D__DEBUG=1  --debugger=pickit3  --double=24 --float=24 --emi=wordwrite --opt=+asm,+asmfile,-speed,+space,-debug,-local --addrqual=ignore --mode=free -P -N255 --warn=0 --asmlist -DXPRJ_default=$(CND_CONF)  --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,-plib $(COMPARISON_BUILD)  --output=-mcof,+elf:multilocs --stack=compiled:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"     -o${OBJECTDIR}/_ext/2141833485/lcd_lowlevel.p1 ../moule_picebs2_students/lcd_lowlevel.c 
	@-${MV} ${OBJECTDIR}/_ext/2141833485/lcd_lowlevel.d ${OBJECTDIR}/_ext/2141833485/lcd_lowlevel.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/_ext/2141833485/lcd_lowlevel.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/_ext/2141833485/main.p1: ../moule_picebs2_students/main.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/2141833485" 
	@${RM} ${OBJECTDIR}/_ext/2141833485/main.p1.d 
	@${RM} ${OBJECTDIR}/_ext/2141833485/main.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  -D__DEBUG=1  --debugger=pickit3  --double=24 --float=24 --emi=wordwrite --opt=+asm,+asmfile,-speed,+space,-debug,-local --addrqual=ignore --mode=free -P -N255 --warn=0 --asmlist -DXPRJ_default=$(CND_CONF)  --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,-plib $(COMPARISON_BUILD)  --output=-mcof,+elf:multilocs --stack=compiled:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"     -o${OBJECTDIR}/_ext/2141833485/main.p1 ../moule_picebs2_students/main.c 
	@-${MV} ${OBJECTDIR}/_ext/2141833485/main.d ${OBJECTDIR}/_ext/2141833485/main.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/_ext/2141833485/main.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/_ext/2141833485/modbus.p1: ../moule_picebs2_students/modbus.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/2141833485" 
	@${RM} ${OBJECTDIR}/_ext/2141833485/modbus.p1.d 
	@${RM} ${OBJECTDIR}/_ext/2141833485/modbus.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  -D__DEBUG=1  --debugger=pickit3  --double=24 --float=24 --emi=wordwrite --opt=+asm,+asmfile,-speed,+space,-debug,-local --addrqual=ignore --mode=free -P -N255 --warn=0 --asmlist -DXPRJ_default=$(CND_CONF)  --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,-plib $(COMPARISON_BUILD)  --output=-mcof,+elf:multilocs --stack=compiled:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"     -o${OBJECTDIR}/_ext/2141833485/modbus.p1 ../moule_picebs2_students/modbus.c 
	@-${MV} ${OBJECTDIR}/_ext/2141833485/modbus.d ${OBJECTDIR}/_ext/2141833485/modbus.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/_ext/2141833485/modbus.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/_ext/2141833485/pwm.p1: ../moule_picebs2_students/pwm.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/2141833485" 
	@${RM} ${OBJECTDIR}/_ext/2141833485/pwm.p1.d 
	@${RM} ${OBJECTDIR}/_ext/2141833485/pwm.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  -D__DEBUG=1  --debugger=pickit3  --double=24 --float=24 --emi=wordwrite --opt=+asm,+asmfile,-speed,+space,-debug,-local --addrqual=ignore --mode=free -P -N255 --warn=0 --asmlist -DXPRJ_default=$(CND_CONF)  --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,-plib $(COMPARISON_BUILD)  --output=-mcof,+elf:multilocs --stack=compiled:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"     -o${OBJECTDIR}/_ext/2141833485/pwm.p1 ../moule_picebs2_students/pwm.c 
	@-${MV} ${OBJECTDIR}/_ext/2141833485/pwm.d ${OBJECTDIR}/_ext/2141833485/pwm.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/_ext/2141833485/pwm.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/_ext/2141833485/tsc.p1: ../moule_picebs2_students/tsc.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/2141833485" 
	@${RM} ${OBJECTDIR}/_ext/2141833485/tsc.p1.d 
	@${RM} ${OBJECTDIR}/_ext/2141833485/tsc.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  -D__DEBUG=1  --debugger=pickit3  --double=24 --float=24 --emi=wordwrite --opt=+asm,+asmfile,-speed,+space,-debug,-local --addrqual=ignore --mode=free -P -N255 --warn=0 --asmlist -DXPRJ_default=$(CND_CONF)  --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,-plib $(COMPARISON_BUILD)  --output=-mcof,+elf:multilocs --stack=compiled:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"     -o${OBJECTDIR}/_ext/2141833485/tsc.p1 ../moule_picebs2_students/tsc.c 
	@-${MV} ${OBJECTDIR}/_ext/2141833485/tsc.d ${OBJECTDIR}/_ext/2141833485/tsc.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/_ext/2141833485/tsc.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/_ext/2141833485/uart.p1: ../moule_picebs2_students/uart.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/2141833485" 
	@${RM} ${OBJECTDIR}/_ext/2141833485/uart.p1.d 
	@${RM} ${OBJECTDIR}/_ext/2141833485/uart.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  -D__DEBUG=1  --debugger=pickit3  --double=24 --float=24 --emi=wordwrite --opt=+asm,+asmfile,-speed,+space,-debug,-local --addrqual=ignore --mode=free -P -N255 --warn=0 --asmlist -DXPRJ_default=$(CND_CONF)  --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,-plib $(COMPARISON_BUILD)  --output=-mcof,+elf:multilocs --stack=compiled:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"     -o${OBJECTDIR}/_ext/2141833485/uart.p1 ../moule_picebs2_students/uart.c 
	@-${MV} ${OBJECTDIR}/_ext/2141833485/uart.d ${OBJECTDIR}/_ext/2141833485/uart.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/_ext/2141833485/uart.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
else
${OBJECTDIR}/_ext/2141833485/adc.p1: ../moule_picebs2_students/adc.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/2141833485" 
	@${RM} ${OBJECTDIR}/_ext/2141833485/adc.p1.d 
	@${RM} ${OBJECTDIR}/_ext/2141833485/adc.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  --double=24 --float=24 --emi=wordwrite --opt=+asm,+asmfile,-speed,+space,-debug,-local --addrqual=ignore --mode=free -P -N255 --warn=0 --asmlist -DXPRJ_default=$(CND_CONF)  --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,-plib $(COMPARISON_BUILD)  --output=-mcof,+elf:multilocs --stack=compiled:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"     -o${OBJECTDIR}/_ext/2141833485/adc.p1 ../moule_picebs2_students/adc.c 
	@-${MV} ${OBJECTDIR}/_ext/2141833485/adc.d ${OBJECTDIR}/_ext/2141833485/adc.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/_ext/2141833485/adc.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/_ext/2141833485/arialNarrow_12ptBitmaps.p1: ../moule_picebs2_students/arialNarrow_12ptBitmaps.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/2141833485" 
	@${RM} ${OBJECTDIR}/_ext/2141833485/arialNarrow_12ptBitmaps.p1.d 
	@${RM} ${OBJECTDIR}/_ext/2141833485/arialNarrow_12ptBitmaps.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  --double=24 --float=24 --emi=wordwrite --opt=+asm,+asmfile,-speed,+space,-debug,-local --addrqual=ignore --mode=free -P -N255 --warn=0 --asmlist -DXPRJ_default=$(CND_CONF)  --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,-plib $(COMPARISON_BUILD)  --output=-mcof,+elf:multilocs --stack=compiled:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"     -o${OBJECTDIR}/_ext/2141833485/arialNarrow_12ptBitmaps.p1 ../moule_picebs2_students/arialNarrow_12ptBitmaps.c 
	@-${MV} ${OBJECTDIR}/_ext/2141833485/arialNarrow_12ptBitmaps.d ${OBJECTDIR}/_ext/2141833485/arialNarrow_12ptBitmaps.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/_ext/2141833485/arialNarrow_12ptBitmaps.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/_ext/2141833485/arialRoundedMTBold18ptBitmaps.p1: ../moule_picebs2_students/arialRoundedMTBold18ptBitmaps.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/2141833485" 
	@${RM} ${OBJECTDIR}/_ext/2141833485/arialRoundedMTBold18ptBitmaps.p1.d 
	@${RM} ${OBJECTDIR}/_ext/2141833485/arialRoundedMTBold18ptBitmaps.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  --double=24 --float=24 --emi=wordwrite --opt=+asm,+asmfile,-speed,+space,-debug,-local --addrqual=ignore --mode=free -P -N255 --warn=0 --asmlist -DXPRJ_default=$(CND_CONF)  --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,-plib $(COMPARISON_BUILD)  --output=-mcof,+elf:multilocs --stack=compiled:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"     -o${OBJECTDIR}/_ext/2141833485/arialRoundedMTBold18ptBitmaps.p1 ../moule_picebs2_students/arialRoundedMTBold18ptBitmaps.c 
	@-${MV} ${OBJECTDIR}/_ext/2141833485/arialRoundedMTBold18ptBitmaps.d ${OBJECTDIR}/_ext/2141833485/arialRoundedMTBold18ptBitmaps.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/_ext/2141833485/arialRoundedMTBold18ptBitmaps.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/_ext/2141833485/configuration_bits.p1: ../moule_picebs2_students/configuration_bits.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/2141833485" 
	@${RM} ${OBJECTDIR}/_ext/2141833485/configuration_bits.p1.d 
	@${RM} ${OBJECTDIR}/_ext/2141833485/configuration_bits.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  --double=24 --float=24 --emi=wordwrite --opt=+asm,+asmfile,-speed,+space,-debug,-local --addrqual=ignore --mode=free -P -N255 --warn=0 --asmlist -DXPRJ_default=$(CND_CONF)  --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,-plib $(COMPARISON_BUILD)  --output=-mcof,+elf:multilocs --stack=compiled:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"     -o${OBJECTDIR}/_ext/2141833485/configuration_bits.p1 ../moule_picebs2_students/configuration_bits.c 
	@-${MV} ${OBJECTDIR}/_ext/2141833485/configuration_bits.d ${OBJECTDIR}/_ext/2141833485/configuration_bits.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/_ext/2141833485/configuration_bits.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/_ext/2141833485/crc.p1: ../moule_picebs2_students/crc.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/2141833485" 
	@${RM} ${OBJECTDIR}/_ext/2141833485/crc.p1.d 
	@${RM} ${OBJECTDIR}/_ext/2141833485/crc.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  --double=24 --float=24 --emi=wordwrite --opt=+asm,+asmfile,-speed,+space,-debug,-local --addrqual=ignore --mode=free -P -N255 --warn=0 --asmlist -DXPRJ_default=$(CND_CONF)  --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,-plib $(COMPARISON_BUILD)  --output=-mcof,+elf:multilocs --stack=compiled:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"     -o${OBJECTDIR}/_ext/2141833485/crc.p1 ../moule_picebs2_students/crc.c 
	@-${MV} ${OBJECTDIR}/_ext/2141833485/crc.d ${OBJECTDIR}/_ext/2141833485/crc.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/_ext/2141833485/crc.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/_ext/2141833485/eeprom.p1: ../moule_picebs2_students/eeprom.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/2141833485" 
	@${RM} ${OBJECTDIR}/_ext/2141833485/eeprom.p1.d 
	@${RM} ${OBJECTDIR}/_ext/2141833485/eeprom.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  --double=24 --float=24 --emi=wordwrite --opt=+asm,+asmfile,-speed,+space,-debug,-local --addrqual=ignore --mode=free -P -N255 --warn=0 --asmlist -DXPRJ_default=$(CND_CONF)  --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,-plib $(COMPARISON_BUILD)  --output=-mcof,+elf:multilocs --stack=compiled:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"     -o${OBJECTDIR}/_ext/2141833485/eeprom.p1 ../moule_picebs2_students/eeprom.c 
	@-${MV} ${OBJECTDIR}/_ext/2141833485/eeprom.d ${OBJECTDIR}/_ext/2141833485/eeprom.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/_ext/2141833485/eeprom.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/_ext/2141833485/interrupts.p1: ../moule_picebs2_students/interrupts.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/2141833485" 
	@${RM} ${OBJECTDIR}/_ext/2141833485/interrupts.p1.d 
	@${RM} ${OBJECTDIR}/_ext/2141833485/interrupts.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  --double=24 --float=24 --emi=wordwrite --opt=+asm,+asmfile,-speed,+space,-debug,-local --addrqual=ignore --mode=free -P -N255 --warn=0 --asmlist -DXPRJ_default=$(CND_CONF)  --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,-plib $(COMPARISON_BUILD)  --output=-mcof,+elf:multilocs --stack=compiled:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"     -o${OBJECTDIR}/_ext/2141833485/interrupts.p1 ../moule_picebs2_students/interrupts.c 
	@-${MV} ${OBJECTDIR}/_ext/2141833485/interrupts.d ${OBJECTDIR}/_ext/2141833485/interrupts.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/_ext/2141833485/interrupts.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/_ext/2141833485/lcd_highlevel.p1: ../moule_picebs2_students/lcd_highlevel.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/2141833485" 
	@${RM} ${OBJECTDIR}/_ext/2141833485/lcd_highlevel.p1.d 
	@${RM} ${OBJECTDIR}/_ext/2141833485/lcd_highlevel.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  --double=24 --float=24 --emi=wordwrite --opt=+asm,+asmfile,-speed,+space,-debug,-local --addrqual=ignore --mode=free -P -N255 --warn=0 --asmlist -DXPRJ_default=$(CND_CONF)  --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,-plib $(COMPARISON_BUILD)  --output=-mcof,+elf:multilocs --stack=compiled:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"     -o${OBJECTDIR}/_ext/2141833485/lcd_highlevel.p1 ../moule_picebs2_students/lcd_highlevel.c 
	@-${MV} ${OBJECTDIR}/_ext/2141833485/lcd_highlevel.d ${OBJECTDIR}/_ext/2141833485/lcd_highlevel.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/_ext/2141833485/lcd_highlevel.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/_ext/2141833485/lcd_lowlevel.p1: ../moule_picebs2_students/lcd_lowlevel.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/2141833485" 
	@${RM} ${OBJECTDIR}/_ext/2141833485/lcd_lowlevel.p1.d 
	@${RM} ${OBJECTDIR}/_ext/2141833485/lcd_lowlevel.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  --double=24 --float=24 --emi=wordwrite --opt=+asm,+asmfile,-speed,+space,-debug,-local --addrqual=ignore --mode=free -P -N255 --warn=0 --asmlist -DXPRJ_default=$(CND_CONF)  --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,-plib $(COMPARISON_BUILD)  --output=-mcof,+elf:multilocs --stack=compiled:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"     -o${OBJECTDIR}/_ext/2141833485/lcd_lowlevel.p1 ../moule_picebs2_students/lcd_lowlevel.c 
	@-${MV} ${OBJECTDIR}/_ext/2141833485/lcd_lowlevel.d ${OBJECTDIR}/_ext/2141833485/lcd_lowlevel.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/_ext/2141833485/lcd_lowlevel.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/_ext/2141833485/main.p1: ../moule_picebs2_students/main.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/2141833485" 
	@${RM} ${OBJECTDIR}/_ext/2141833485/main.p1.d 
	@${RM} ${OBJECTDIR}/_ext/2141833485/main.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  --double=24 --float=24 --emi=wordwrite --opt=+asm,+asmfile,-speed,+space,-debug,-local --addrqual=ignore --mode=free -P -N255 --warn=0 --asmlist -DXPRJ_default=$(CND_CONF)  --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,-plib $(COMPARISON_BUILD)  --output=-mcof,+elf:multilocs --stack=compiled:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"     -o${OBJECTDIR}/_ext/2141833485/main.p1 ../moule_picebs2_students/main.c 
	@-${MV} ${OBJECTDIR}/_ext/2141833485/main.d ${OBJECTDIR}/_ext/2141833485/main.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/_ext/2141833485/main.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/_ext/2141833485/modbus.p1: ../moule_picebs2_students/modbus.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/2141833485" 
	@${RM} ${OBJECTDIR}/_ext/2141833485/modbus.p1.d 
	@${RM} ${OBJECTDIR}/_ext/2141833485/modbus.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  --double=24 --float=24 --emi=wordwrite --opt=+asm,+asmfile,-speed,+space,-debug,-local --addrqual=ignore --mode=free -P -N255 --warn=0 --asmlist -DXPRJ_default=$(CND_CONF)  --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,-plib $(COMPARISON_BUILD)  --output=-mcof,+elf:multilocs --stack=compiled:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"     -o${OBJECTDIR}/_ext/2141833485/modbus.p1 ../moule_picebs2_students/modbus.c 
	@-${MV} ${OBJECTDIR}/_ext/2141833485/modbus.d ${OBJECTDIR}/_ext/2141833485/modbus.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/_ext/2141833485/modbus.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/_ext/2141833485/pwm.p1: ../moule_picebs2_students/pwm.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/2141833485" 
	@${RM} ${OBJECTDIR}/_ext/2141833485/pwm.p1.d 
	@${RM} ${OBJECTDIR}/_ext/2141833485/pwm.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  --double=24 --float=24 --emi=wordwrite --opt=+asm,+asmfile,-speed,+space,-debug,-local --addrqual=ignore --mode=free -P -N255 --warn=0 --asmlist -DXPRJ_default=$(CND_CONF)  --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,-plib $(COMPARISON_BUILD)  --output=-mcof,+elf:multilocs --stack=compiled:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"     -o${OBJECTDIR}/_ext/2141833485/pwm.p1 ../moule_picebs2_students/pwm.c 
	@-${MV} ${OBJECTDIR}/_ext/2141833485/pwm.d ${OBJECTDIR}/_ext/2141833485/pwm.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/_ext/2141833485/pwm.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/_ext/2141833485/tsc.p1: ../moule_picebs2_students/tsc.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/2141833485" 
	@${RM} ${OBJECTDIR}/_ext/2141833485/tsc.p1.d 
	@${RM} ${OBJECTDIR}/_ext/2141833485/tsc.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  --double=24 --float=24 --emi=wordwrite --opt=+asm,+asmfile,-speed,+space,-debug,-local --addrqual=ignore --mode=free -P -N255 --warn=0 --asmlist -DXPRJ_default=$(CND_CONF)  --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,-plib $(COMPARISON_BUILD)  --output=-mcof,+elf:multilocs --stack=compiled:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"     -o${OBJECTDIR}/_ext/2141833485/tsc.p1 ../moule_picebs2_students/tsc.c 
	@-${MV} ${OBJECTDIR}/_ext/2141833485/tsc.d ${OBJECTDIR}/_ext/2141833485/tsc.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/_ext/2141833485/tsc.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/_ext/2141833485/uart.p1: ../moule_picebs2_students/uart.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/2141833485" 
	@${RM} ${OBJECTDIR}/_ext/2141833485/uart.p1.d 
	@${RM} ${OBJECTDIR}/_ext/2141833485/uart.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  --double=24 --float=24 --emi=wordwrite --opt=+asm,+asmfile,-speed,+space,-debug,-local --addrqual=ignore --mode=free -P -N255 --warn=0 --asmlist -DXPRJ_default=$(CND_CONF)  --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,-plib $(COMPARISON_BUILD)  --output=-mcof,+elf:multilocs --stack=compiled:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"     -o${OBJECTDIR}/_ext/2141833485/uart.p1 ../moule_picebs2_students/uart.c 
	@-${MV} ${OBJECTDIR}/_ext/2141833485/uart.d ${OBJECTDIR}/_ext/2141833485/uart.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/_ext/2141833485/uart.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
endif

# ------------------------------------------------------------------------------------
# Rules for buildStep: assemble
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
else
endif

# ------------------------------------------------------------------------------------
# Rules for buildStep: link
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
dist/${CND_CONF}/${IMAGE_TYPE}/MyModbusServer.X.${IMAGE_TYPE}.${OUTPUT_SUFFIX}: ${OBJECTFILES}  nbproject/Makefile-${CND_CONF}.mk    
	@${MKDIR} dist/${CND_CONF}/${IMAGE_TYPE} 
	${MP_CC} $(MP_EXTRA_LD_PRE) --chip=$(MP_PROCESSOR_OPTION) -G -mdist/${CND_CONF}/${IMAGE_TYPE}/MyModbusServer.X.${IMAGE_TYPE}.map  -D__DEBUG=1  --debugger=pickit3  -DXPRJ_default=$(CND_CONF)  --double=24 --float=24 --emi=wordwrite --opt=+asm,+asmfile,-speed,+space,-debug,-local --addrqual=ignore --mode=free -P -N255 --warn=0 --asmlist --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,-plib --output=-mcof,+elf:multilocs --stack=compiled:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"        $(COMPARISON_BUILD) --memorysummary dist/${CND_CONF}/${IMAGE_TYPE}/memoryfile.xml -odist/${CND_CONF}/${IMAGE_TYPE}/MyModbusServer.X.${IMAGE_TYPE}.${DEBUGGABLE_SUFFIX}  ${OBJECTFILES_QUOTED_IF_SPACED}     
	@${RM} dist/${CND_CONF}/${IMAGE_TYPE}/MyModbusServer.X.${IMAGE_TYPE}.hex 
	
else
dist/${CND_CONF}/${IMAGE_TYPE}/MyModbusServer.X.${IMAGE_TYPE}.${OUTPUT_SUFFIX}: ${OBJECTFILES}  nbproject/Makefile-${CND_CONF}.mk   
	@${MKDIR} dist/${CND_CONF}/${IMAGE_TYPE} 
	${MP_CC} $(MP_EXTRA_LD_PRE) --chip=$(MP_PROCESSOR_OPTION) -G -mdist/${CND_CONF}/${IMAGE_TYPE}/MyModbusServer.X.${IMAGE_TYPE}.map  -DXPRJ_default=$(CND_CONF)  --double=24 --float=24 --emi=wordwrite --opt=+asm,+asmfile,-speed,+space,-debug,-local --addrqual=ignore --mode=free -P -N255 --warn=0 --asmlist --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,-plib --output=-mcof,+elf:multilocs --stack=compiled:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"     $(COMPARISON_BUILD) --memorysummary dist/${CND_CONF}/${IMAGE_TYPE}/memoryfile.xml -odist/${CND_CONF}/${IMAGE_TYPE}/MyModbusServer.X.${IMAGE_TYPE}.${DEBUGGABLE_SUFFIX}  ${OBJECTFILES_QUOTED_IF_SPACED}     
	
endif


# Subprojects
.build-subprojects:


# Subprojects
.clean-subprojects:

# Clean Targets
.clean-conf: ${CLEAN_SUBPROJECTS}
	${RM} -r build/default
	${RM} -r dist/default

# Enable dependency checking
.dep.inc: .depcheck-impl

DEPFILES=$(shell mplabwildcard ${POSSIBLE_DEPFILES})
ifneq (${DEPFILES},)
include ${DEPFILES}
endif
