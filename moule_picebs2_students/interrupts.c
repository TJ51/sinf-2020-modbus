/******************************************************************************/
/*Files to Include                                                            */
/******************************************************************************/

#if defined(__XC)
    #include <xc.h>         /* XC8 General Include File */
#elif defined(HI_TECH_C)
    #include <htc.h>        /* HiTech General Include File */
#elif defined(__18CXX)
    #include <p18cxxx.h>    /* C18 General Include File */
#endif

#if defined(__XC) || defined(HI_TECH_C)

#include <stdint.h>         /* For uint8_t definition */
#include <stdbool.h>        /* For true/false definition */
#include "tsc.h"
#include "modbus.h"
#include "uart.h"
#endif

/******************************************************************************/
/* Interrupt Routines                                                         */
/******************************************************************************/
/* High-priority service */

void interrupt high_isr(void)
{
	// TODO ...
}

