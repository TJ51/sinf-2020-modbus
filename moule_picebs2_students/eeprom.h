/* 
 * File:   eeprom.h
 * Author: alex
 *
 * Created on 2. d�cembre 2015, 11:37
 */

#ifndef EEPROM_H
#define	EEPROM_H

#ifdef	__cplusplus
extern "C" {
#endif

    #include <stdint.h>
    
    void ee_write(uint16_t addr, uint8_t value);
    uint8_t ee_read(uint16_t addr);



#ifdef	__cplusplus
}
#endif

#endif	/* EEPROM_H */

