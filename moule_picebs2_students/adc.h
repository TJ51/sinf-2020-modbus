/************************************************************************************/
/* FILE		      : adc.h   									                                   	    */
/* AUTHOR		    : you																																*/
/*----------------------------------------------------------------------------------*/
/* COMMENTS     : Offers ADC function ued on PIC18F6680	and PICEBS board     				*/
/*----------------------------------------------------------------------------------*/
/* REVISION     : 1.0 (02-2013) - tested with HI-TECH compiler                      */
/************************************************************************************/
#include <htc.h>
#include <stdint.h>

#ifndef  ADC_H
#define ADC_H

/************************************************************************************/
/* FUNCTION     : ADC_init									                                   	    */
/* INPUT		    : -																																	*/
/* OUTPUT		    : - 																																*/
/*----------------------------------------------------------------------------------*/
/* COMMENTS     : Initialise th ADC for use of channels 0 and 1					   			    */
/************************************************************************************/
void ADC_init(void);
/************************************************************************************/
/* FUNCTION     : ADC_measure    			                                         	    */
/* INPUT				: uint8_t channel	-> channel to measure                  						*/
/* OUTPUT				: uint16_t -> return the 10 bits measured value     								*/
/*----------------------------------------------------------------------------------*/
/* COMMENTS     : Execute an ADC measure on the selected channel and waits until    */
/*                the conversion is finished.                                       */ 
/************************************************************************************/
uint16_t ADC_measure(uint8_t channel);
#endif
