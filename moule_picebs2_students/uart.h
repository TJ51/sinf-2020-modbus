/************************************************************************************/
/* FILE		      : uart.h   									                                   	    */
/* AUTHOR		    : you																																*/
/*----------------------------------------------------------------------------------*/
/* COMMENTS     : Offers UART function ued on PIC18F6680	and PICEBS board     			*/
/*                -->> CPU frequency is 25MHz <<---																	*/
/*----------------------------------------------------------------------------------*/
/* REVISION     : 1.0 (02-2013) - tested with HI-TECH compiler                      */
/************************************************************************************/
#include <htc.h>
#include <stdint.h>

#ifndef  UART_H
#define UART_H

extern	uint8_t	rxBuffer[40];
extern	uint8_t	txBuffer[40];

/************************************************************************************/
/* FUNCTION     : UART_init									                                   	    */
/* INPUT		    : -																																	*/
/* OUTPUT		    : - 																																*/
/*----------------------------------------------------------------------------------*/
/* COMMENTS     : Initialise the UART with the following parameters                 */
/*              	9'600 bds, 1 start bit, 1 stop bit, 9 data bits (last is 2nd stop)*/
/*                The UART is initialised to use the receive interrupt              */
/************************************************************************************/
void UART_init(void);

/************************************************************************************/
/* FUNCTION     : UART_write     			                                         	    */
/* INPUT				: uint8_t data	-> byte to send tu uart interface           				*/
/* OUTPUT				: -																								  								*/
/*----------------------------------------------------------------------------------*/
/* COMMENTS     : Send the data byte through the  serial line  											*/
/************************************************************************************/
void UART_write(uint8_t data);

/************************************************************************************/
/* FUNCTION     : UART_read     			                                         	    */
/* INPUT				: - 																							          				*/
/* OUTPUT				: returns the received byte												  								*/
/*----------------------------------------------------------------------------------*/
/* COMMENTS     : This function has to be called in RX_USART_ISR event 							*/
/************************************************************************************/
uint8_t UART_read(void);

#endif
